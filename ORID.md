# Daily Report (2023/07/24)
# O (Objective): 
## 1. What did we learn today?
### We learned Cloud Native, Html & Css & Js, React and Jsx today.

## 2. What activities did you do? 
### I conducted a code review and drew a concept map of "What have we learnt about Spring Boot" with my team members and did some exercises to learn React.

## 3. What scenes have impressed you?
### I have been impressed by the React learning today.
---------------------

# R (Reflective): 
## Please use one word to express your feelings about today's class.
---------------------
### Interesting.

# I (Interpretive):
## What do you think about this? What was the most meaningful aspect of this activity?
---------------------
### I think the most meaningful aspect of this activity is drawing a concept map with my team members today.Because this was the result of our joint discussion, although time control was not good enough to present the results well, it also indicated where our team needs to make progress.

# D (Decisional): 
## Where do you most want to apply what you have learned today? What changes will you make?
---------------------
### I will use today's game to learn front-end knowledge.
### I will develop a good coding habit of committing step by step and learn the basic syntax of jsx and react tonight.
### And I will strengthen communication with group members when doing group assignments in order to set aside time to rehearse the content of the speech in advance and present better performance results.



