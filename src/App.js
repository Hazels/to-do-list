import './App.css';
import Navigator from './pages/Navigator';
import { Outlet } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Navigator></Navigator>
      <Outlet></Outlet>
    </div>
  )
}

export default App;
