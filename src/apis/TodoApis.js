import axios from "axios";

const instance = axios.create({
    baseURL: 'https://64c0b66a0d8e251fd1126452.mockapi.io/api',
});
const instanceNew = axios.create({
    baseURL: 'http://localhost:8080',
})
export const getTodo = () => {
    return instanceNew.get('/todos')
}

export const getTodoById = (id) => {
    return instanceNew.get(`/todos/${id}`)
}

export const postTodo = (text) => {
    return instanceNew.post('/todos', {
        text,
    })
}
export const updateTodo = (id, done) => {
    return instanceNew.put(`/todos/${id}`, {
        done
    })
}

export const updateText = (id, text) => {
    return instanceNew.put(`/todos/${id}`, {
        text
    })
}
export const deleteTodo = (id) => {
    return instanceNew.delete(`/todos/${id}`)
}

