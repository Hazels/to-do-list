import { useDispatch } from 'react-redux';
import { getTodo, postTodo, updateTodo, deleteTodo, updateText } from './../../apis/TodoApis';
import { loadItems } from '../../features/todoList/todoItemSlice';

const useTodos = (() => {

    const dispatch = useDispatch()
    const getTodos = async () => {
        const { data } = await getTodo()
        dispatch(loadItems(data))
    }

    const postTodos = async (text) => {
        await postTodo(text)
        getTodos()
    }

    const updateTodos = async (id, done) => {
        await updateTodo(id, done)
        getTodos()
    }

    const deleteTodos = async (id) => {
        await deleteTodo(id)
        getTodos()
    }
    const updateTodoText = async (id, text) => {
        await updateText(id, text)
        getTodos()
    }
    return {
        getTodos,
        postTodos,
        updateTodos,
        deleteTodos,
        updateTodoText
    }
})

export default useTodos