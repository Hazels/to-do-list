import { configureStore } from '@reduxjs/toolkit';
import todoReducer from '../features/todoList/todoItemSlice';

export default configureStore({
  reducer: {
    todoItem: todoReducer,
  },
})