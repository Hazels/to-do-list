import { useEffect } from 'react';
import { useSelector } from "react-redux";
import useTodos from '../app/hooks/useTodos';
import { Divider, List } from 'antd';
import TodoGenerator from './todoGenerator/TodoGenerator';
import { HeartTwoTone, SmileTwoTone } from '@ant-design/icons';
import TodoItem from "./todoItem/TodoItem";

const TodoGroup = () => {
  const todoItemsList = useSelector((state) => state.todoItem.todoList)

  const { getTodos } = useTodos()

  useEffect(() => {
    getTodos()
  }, [])
  const style = { margin: 'auto', width: '50%' }

  return (
    <div style={style}>
      <Divider orientation="left"></Divider>
      <List
        header={<div><SmileTwoTone />&nbsp;&nbsp;&nbsp;&nbsp;
          <HeartTwoTone twoToneColor="#eb2f96" />
          <h3>Todo List</h3></div>}
        footer={<TodoGenerator></TodoGenerator>
        }
        bordered
        dataSource={todoItemsList}
        renderItem={(todoItem) => (
          <List.Item>
            <TodoItem todoItem={todoItem} key={todoItem.id}></TodoItem>
          </List.Item>
        )}
      />
      {/* {

        todoItemsList?.map((todoItem) =>
          <TodoItem todoItem={todoItem} key={todoItem.id}></TodoItem>
        )
      } */}
    </div>
  )
}

export default TodoGroup