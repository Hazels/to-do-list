import { Button, Input } from 'antd';
import React, { useState } from 'react';
import useTodos from '../../app/hooks/useTodos';
import './TodoGenerator.css';

const TodoGenerator = () => {

  const { postTodos } = useTodos()
  const [currentValue, setCurrentValue] = useState([])
  const handleAdd = (event) => {
    setCurrentValue(event.target.value)
  }
  const handleSubmit = () => {
    if (currentValue == '') {
      alert('please enter a todo here!')
    }
    else {
      postTodos(currentValue)
      setCurrentValue("")
    }
  }

  const style = {width:'50%',height:'50px'}

  return (

    <div>
      <Input showCount maxLength={50} style={style} value={currentValue}
        onChange={handleAdd} allowClear={true} placeholder='input a new todo here...' />
      &nbsp;
      <Button type="primary" onClick={handleSubmit} style={{top:'10px',left:'10px'}}>add</Button>
    </div>
  )
}

export default TodoGenerator