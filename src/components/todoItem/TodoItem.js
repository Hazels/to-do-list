import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import useTodos from '../../app/hooks/useTodos';
import './ToIdotem.css';
import { Modal, Input } from 'antd';

const TodoItem = ({ todoItem }) => {
  const { updateTodos, deleteTodos , updateTodoText } = useTodos()
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [updateText, setUpdateText] = useState("");

  const showModal = () => {
    setIsModalOpen(true);
    setUpdateText(todoItem.text)
  };
  const handleOk = () => {
    updateTodoText(todoItem.id,updateText)
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const handleChangeInput = (event) => {
    setUpdateText(event.target.value)
  }

  const handleChangeButton = () => {
    deleteTodos(todoItem.id)
  }

  const handleChangeLine = () => {
    updateTodos(todoItem.id, !todoItem.done)
  }

  return (
    <div className={todoItem.done ? 'done' : 'undone'}>
      <span onClick={handleChangeLine} > {todoItem.text}
      </span >
      <DeleteOutlined onClick={handleChangeButton} spin={true} className='button' />
      <EditOutlined className='update' onClick={showModal} />

      <Modal title="Update Todo" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <Input style={{ height: '100px' }} maxLength={50} showCount allowClear value={updateText}
          onChange={handleChangeInput} />
      </Modal>
    </div>
  )
}

export default TodoItem