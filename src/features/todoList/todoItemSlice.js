import { createSlice } from '@reduxjs/toolkit'

export const todoItemSlice = createSlice({
  name: 'todoItem',
  initialState: {
    todoList: [],
  },
  reducers: {
    loadItems:(state, action)=>{
      state.todoList = action.payload
    },
  },
})

export const { loadItems } = todoItemSlice.actions

export default todoItemSlice.reducer