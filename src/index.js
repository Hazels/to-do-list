import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import store from './app/store'
import { Provider } from 'react-redux'
import { createBrowserRouter,RouterProvider } from 'react-router-dom'
import HelpPage from './pages/HelpPage'
import ErrorPage from './pages/404Page'
import DoneListPage from './pages/DoneListPage'
import TodoList from './components/TodoList'
import DetailPage from './pages/DetailPage'

// As of React 18
const root = ReactDOM.createRoot(document.getElementById('root'))
const router = createBrowserRouter([
  {
    path:'/',
    element:<App></App>,
    children:[
      {
        index:true,
        element:<TodoList></TodoList>
      },
      {
        path:'/help',
        element:<HelpPage></HelpPage>
      },
      {
        path:'/doneList',
        element:<DoneListPage></DoneListPage>
      },
      {
        path:'/doneList/:id',
        element:<DetailPage></DetailPage>
      },
    ]
  },
  {
    path:'*',
    element:<ErrorPage></ErrorPage>
  },
])
root.render(
  <Provider store={store}>
    <RouterProvider router={router}></RouterProvider>
  </Provider>
)