import React from 'react'
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom'
import './DetailPage.css'
const DetailPage = (() => {
    const { id } = useParams();
    const done = useSelector((state) => state.todoItem.todoList).find(item => item.id === id)
    console.log(done)
   

    return (
        
    <div style={{   
        textAlign:'center',
        position: 'absolute',
        top: '10%',
        left:'50%'}} >
        <div>id:  {done.id}</div>
        <div>text:  {done.text}</div>
    </div>
)
})

export default DetailPage