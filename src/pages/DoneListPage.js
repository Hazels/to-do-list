import { CheckCircleTwoTone } from '@ant-design/icons';
import { Divider, List } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';


const DoneListPage = (() => {
    const doneList = useSelector((state) => state.todoItem.todoList).filter(item => item.done)
    const navigate = useNavigate();
    const handleClick = (id) => {
        navigate(`/doneList/${id}`)
    }
    const style = { margin: 'auto', width: '50%' }
    return (
        <div style={style}>
            <Divider orientation="center"></Divider>
            <List
                header={<div>    <CheckCircleTwoTone twoToneColor="#52c41a" />
                    <h3>Done List</h3></div>}
                bordered
                dataSource={doneList}
                renderItem={(item) => (
                    <List.Item style={{
                        textAlign: 'center',
                        margin: 'auto',
                        width: '300px',
                        height: '60px',
                    }}>
                        <div style={{textAlign: 'center',}} key={item.id} onClick={() => handleClick(item.id)}>
                            <input style={{
                                textAlign: 'center',
                                margin: 'auto',
                                width: '250px',
                                height: '40px',
                                backgroundColor:'white',
                                border:'none'
                            }} value={item.text} disabled></input>
                        </div>
                    </List.Item>
                )
                }
            />
        </div >
        // <div className='detail'>
        //     {doneList.map((item) => {
        //         return (
        //             <div key={item.id} onClick={() => handleClick(item.id)}>
        //                 <input value={item.text} disabled></input>
        //             </div>
        //         )
        //     })
        //     }
        // </div>
    )
}
)

export default DoneListPage