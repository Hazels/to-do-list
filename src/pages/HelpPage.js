import React from 'react'
import { Card } from 'antd';
const { Meta } = Card;

const HelpPage = () => (
  <div>
    <Card
      hoverable
      style={{
        position: 'absolute',
        margin: 'auto',
        top: 80,
        left: 700,
      }}
      cover={<img alt="example" src="https://img2.baidu.com/it/u=2121898692,2845085279&fm=253&fmt=auto&app=120&f=JPEG?w=428&h=863" />}
    >
      <Meta title="I love working" description="I love React" />
    </Card>
  </div>
);
export default HelpPage