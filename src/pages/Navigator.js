import { MailOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import './Navigator.css';


function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}

const Navigator = (() => {
  const navigate = useNavigate();

  const items = [
      getItem('Home Page'),
      getItem('Help Page'),
      getItem('Done List')    
  ];

  const handleClick = (id) => {
    navigate(`/doneList/${id}`)
  }


  const handleChangeNav = (e) => {
    if (e.key == 'tmp-0') {
      navigate(`/`)
    }
    else if (e.key == 'tmp-1') {
      navigate(`/help`)
    }
    else if (e.key == 'tmp-2') {
      navigate(`/doneList`)
    }

  }
  return (
    <Menu
      style={{
        width: 250,
        display:'-webkit-box',
        float: 'left'
      }}
      defaultOpenKeys={['sub1']}
      mode="inline"
      items={items}
      onClick={handleChangeNav}
    />

  )
})

export default Navigator